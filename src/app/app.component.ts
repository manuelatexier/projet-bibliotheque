import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){
  // Initialize Firebase
  const config = {
  apiKey: "AIzaSyCq25Q0JlRwDlOI9mN6Db8Bp9IDqYYYlpU",
  authDomain: "projet-bibliotheque-9517f.firebaseapp.com",
  databaseURL: "https://projet-bibliotheque-9517f.firebaseio.com",
  projectId: "projet-bibliotheque-9517f",
  storageBucket: "projet-bibliotheque-9517f.appspot.com",
  messagingSenderId: "668879765600"
};
  firebase.initializeApp(config);
  }

}
