/**
 * Created by clari on 20/08/2018.
 */

export class Book {
  photo: string;
  synopsis: string;
  constructor (public title: string, public author: string){}
}
